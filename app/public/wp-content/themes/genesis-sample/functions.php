<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://www.studiopress.com/
 */

// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

// Sets up the Theme.
require_once get_stylesheet_directory() . '/lib/theme-defaults.php';

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function genesis_sample_localization_setup() {

	load_child_theme_textdomain( genesis_get_theme_handle(), get_stylesheet_directory() . '/languages' );

}

// Adds helper functions.
require_once get_stylesheet_directory() . '/lib/helper-functions.php';

// Adds image upload and color select to Customizer.
require_once get_stylesheet_directory() . '/lib/customize.php';

// Includes Customizer CSS.
require_once get_stylesheet_directory() . '/lib/output.php';

// Adds WooCommerce support.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php';

// Adds the required WooCommerce styles and Customizer CSS.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php';

// Adds the Genesis Connect WooCommerce notice.
require_once get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php';

add_action( 'after_setup_theme', 'genesis_child_gutenberg_support' );
/**
 * Adds Gutenberg opt-in features and styling.
 *
 * @since 2.7.0
 */
function genesis_child_gutenberg_support() { // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedFunctionFound -- using same in all child themes to allow action to be unhooked.
	require_once get_stylesheet_directory() . '/lib/gutenberg/init.php';
}

// Registers the responsive menus.
if ( function_exists( 'genesis_register_responsive_menus' ) ) {
	genesis_register_responsive_menus( genesis_get_config( 'responsive-menus' ) );
}

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function genesis_sample_enqueue_scripts_styles() {

	$appearance = genesis_get_config( 'appearance' );

	wp_enqueue_style(
		genesis_get_theme_handle() . '-fonts',
		$appearance['fonts-url'],
		[],
		genesis_get_theme_version()
	);

	wp_enqueue_style( 'dashicons' );

	if ( genesis_is_amp() ) {
		wp_enqueue_style(
			genesis_get_theme_handle() . '-amp',
			get_stylesheet_directory_uri() . '/lib/amp/amp.css',
			[ genesis_get_theme_handle() ],
			genesis_get_theme_version()
		);
	}

}

add_action( 'after_setup_theme', 'genesis_sample_theme_support', 9 );
/**
 * Add desired theme supports.
 *
 * See config file at `config/theme-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_theme_support() {

	$theme_supports = genesis_get_config( 'theme-supports' );

	foreach ( $theme_supports as $feature => $args ) {
		add_theme_support( $feature, $args );
	}

}

add_action( 'after_setup_theme', 'genesis_sample_post_type_support', 9 );
/**
 * Add desired post type supports.
 *
 * See config file at `config/post-type-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_post_type_support() {

	$post_type_supports = genesis_get_config( 'post-type-supports' );

	foreach ( $post_type_supports as $post_type => $args ) {
		add_post_type_support( $post_type, $args );
	}

}

// Adds image sizes.
add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'genesis-singular-images', 702, 526, true );

// Removes header right widget area.
unregister_sidebar( 'header-right' );

// Removes secondary sidebar.
unregister_sidebar( 'sidebar-alt' );

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Repositions primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header', 'genesis_do_nav', 12 );

// Repositions the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
/**
 * Reduces secondary navigation menu to one level depth.
 *
 * @since 2.2.3
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' === $args['theme_location'] ) {
		$args['depth'] = 1;
	}

	return $args;

}

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function genesis_sample_author_box_gravatar( $size ) {

	return 90;

}

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;
	return $args;

}

// CUSTOM
// -------------------------------------------------

// Reposition the primary navigation menu
// remove_action( 'genesis_after_header', 'genesis_do_nav' );
// add_action( 'genesis_site_title', 'genesis_do_nav',-1000 );

add_action( 'init', 'cd_register_shortcode' );
/**
* Register shortcode with the theme
*/
function cd_register_shortcode() {
  add_shortcode( 'genesis_header', 'cd_search_shortcode' );
}

//* Insert a Genesis search form above the primary sidebar widget area (in lieu of using a widget due to secondary nav or other bing placed above the sidebar) */

add_action( 'genesis_header', 'rpgbundle_search' );

function rpgbundle_search() {
  echo '<div class="site-header-search">';
	get_search_form();
	echo '</div>';
}

// REMOVE H1 SITE TITLE
// -------------------------------------------------

add_filter( 'genesis_pre_get_option_home_h1_on', '__return_true' );


// REMOVE Product side bar
// -------------------------------------------------
add_filter( 'genesis_pre_get_option_site_layout', 'full_width_layout_product_pages' );
function full_width_layout_product_pages( $opt ) {
if ( is_product() ) {
    $opt = 'full-width-content'; 
    return $opt;
    } 
}


/**
 * Add WooCommerce cart to primary navigation Genesis
 * @author      Frank Schrijvers
 * @link        https://www.wpstud.io/add-woocommerce-cart-to-genesis-navigation/
 */
if ( class_exists( 'WooCommerce' ) ) {
    add_action( 'genesis_header_right', 'wpstudio_add_cart' );
    function wpstudio_add_cart() {

		echo '<a href="' . WC()->cart->get_cart_url() .'"title="View your shopping cart" class="mini-cart dash-icon-before">';
		echo '<div class="cart-count">' . WC()->cart->cart_contents_count  . '</div>';
		echo '<div>Cart</div>';

		echo '</a>';
    }
}


// Move tabs
// -------------------------------------------------
  
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

add_action( 'woocommerce_after_single_product_summary', 'wc_output_long_description', 10 );
add_action( 'woocommerce_after_single_product_summary', 'comments_template', 50 );
  


// Remove You are here from breadcrumbs
// -------------------------------------------------

add_filter('genesis_breadcrumb_args', 'remove_breadcrumbs_yourarehere_text');
function remove_breadcrumbs_yourarehere_text( $args ) {
    $args['labels']['prefix'] = '';
    return $args;
}

// Change home
// -------------------------------------------------

add_filter('genesis_breadcrumb_args', 'modify_home_text_breadcrumbs');

function modify_home_text_breadcrumbs($args) {
    $args['home'] = 'Front Page';
    return $args;
}

add_action( 'genesis_archive_title_descriptions', 'woocommerce_taxonomy_archive_description', 10 );

/* Remove the Archive Headline and Text from the default location */

remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_intro_text', 12);
/* Now, add it back at the bottom */
add_action( 'woocommerce_after_shop_loop', 'ar_add_custom_hook_archive_description', 8);
add_action( 'ar_add_wc_archive_descr_text' , 'ar_do_custom_archive_description_text', 7, 3);
function ar_add_custom_hook_archive_description() {

	global $wp_query;

	if ( ! is_category() && ! is_tag() && ! is_tax() ) {
		return;
	}

	$term = is_tax() ? get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ) : $wp_query->get_queried_object();

	if ( ! $term ) {
		return;
	}

	$intro_text = get_term_meta( $term->term_id, 'intro_text', true );
	$intro_text = apply_filters( 'genesis_term_intro_text_output', $intro_text ? $intro_text : '' );

	
	do_action( 'ar_add_wc_archive_descr_text',  $heading, $intro_text, 'taxonomy-archive-description' );
	
	}
function ar_do_custom_archive_description_text( $heading = '', $intro_text = '', $context = '' ) {
	if ( $heading || $intro_text ) {

		genesis_markup( array(
			'open'    => '<div %s>',
			'context' => $context,
		) );

	}	
	if ( $context && $intro_text ) {
		echo $intro_text;
	}
	if ( $heading || $intro_text ) {

		genesis_markup( array(
			'close'   => '</div>',
			'context' => $context,
		) );

	}	
}

//show attributes after summary in product single view
add_action('woocommerce_single_product_summary', function() {
	//template for this is in storefront-child/woocommerce/single-product/product-attributes.php
	global $product;
	echo $product->list_attributes();
}, 25);


//Remove QTY from product page
function custom_remove_all_quantity_fields( $return, $product ) {return true;}
add_filter( 'woocommerce_is_sold_individually','custom_remove_all_quantity_fields', 10, 2 );

//Change onsale name to to show percentages
add_filter('woocommerce_sale_flash', 'woocommerce_custom_sale_text', 10, 3);
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_custom_sale_text', 25 );
function woocommerce_custom_sale_text() {
    global $product;
    if ( ! $product->is_on_sale() ) return;
    if ( $product->is_type( 'simple' ) ) {
        $max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
    } elseif ( $product->is_type( 'variable' ) ) {
        $max_percentage = 0;
        foreach ( $product->get_children() as $child_id ) {
            $variation = wc_get_product( $child_id );
            $price = $variation->get_regular_price();
            $sale = $variation->get_sale_price();
            if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;
            if ( $percentage > $max_percentage ) {
                $max_percentage = $percentage;
            }
        }
    }
    if ( $max_percentage > 0 ) echo "<span class='onsale'>Save " . round($max_percentage) . " %</span>";
}


//* Add Google Font in Genesis - http://www.basicwp.com/?p=2054
add_action( 'wp_enqueue_scripts', 'genesis_google_icon' );
function genesis_google_icon() {
	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/icon?family=Material+Icons', array() );
}


//* Remove add to cart on category pages
add_action( 'woocommerce_after_shop_loop_item', function(){
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
}, 1 );

//* Move sold by on category pages
remove_action( 'woocommerce_after_shop_loop_item', array('WCV_Vendor_Shop', 'template_loop_sold_by'), 9 );
add_action( 'woocommerce_after_shop_loop_item_title', array('WCV_Vendor_Shop', 'template_loop_sold_by'), 0 );

/* Remove Categories from Single Products */ 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

// Remove Empty Tabs
add_filter( 'woocommerce_product_tabs', 'yikes_woo_remove_empty_tabs', 20, 1 );

function yikes_woo_remove_empty_tabs( $tabs ) {

	if ( ! empty( $tabs ) ) {
		foreach ( $tabs as $title => $tab ) {
			if ( empty( $tab['content'] ) && strtolower( $tab['title'] ) !== 'description' ) {
				unset( $tabs[ $title ] );
			}
		}
	}
	return $tabs;
}

function wc_output_long_description() {
	?>
	   <div class="woocommerce-tabs">
	   <?php the_content(); ?>
	   </div>
	<?php
	}